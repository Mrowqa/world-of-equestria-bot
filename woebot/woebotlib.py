#!/usr/bin/env python

# I knew I could generate nicks, but... it's easier :)
import woeuserlib, random, nicks


class WoeBot:
	def __init__(self):
		self.bots = []
		self.users = []
		random.seed()
		
	# bot.addUser('user', 'pass', doAction='bakery', travelTo='crystal', buyItem=('crystal_spd1',3), useItem=('crystal_str1',2))
	def addUser(self, username, password, **tasks):
		user = woeuserlib.WoeUser()
		done = user.login(username, password)
		print user.getNewLog()[:-1]
		if not done:
			return False
		user.tasks = tasks
		self.users.append(user)
		return True
	
	# bot.addBots('bot', range(10), 'pass', {...})
	def addBots(self, username, usernameSufix, password, **tasks):
		botCounter = 0
		if not usernameSufix:
			usernameSufix = ['']
		sufixInfo = [username + "%s" % usernameSufix[0], username + "%s" % usernameSufix[-1]]
		random.shuffle(usernameSufix) # for random/ perfomance...
		for sufix in usernameSufix:
			bot = woeuserlib.WoeUser()
			if not bot.login(username + "%s" % sufix, password + "%s" % sufix):
				continue
			bot.tasks = tasks
			self.bots.append(bot)
			botCounter += 1
		print "WoeBot:addBots()> Bots logged in/all bots: %d/%d (%.2f%%) [%s .. %s]" % (botCounter, len(usernameSufix), float(botCounter) / len(usernameSufix) * 100, sufixInfo[0], sufixInfo[-1])
		return botCounter
	
	def registerBots(self, username, usernameSufix, password):
		botCounter = 0
		i = 0
		sufixInfo = [username + "%s" % usernameSufix[0], username + "%s" % usernameSufix[-1]]
		random.shuffle(usernameSufix) # for random/ perfomance...
		for sufix in usernameSufix:
			i += 1
			bot = woeuserlib.WoeUser()
			nick = random.choice(nicks.nicks) + (random.choice(nicks.nicks) if random.randint(0, 1) else "")
			email = nick.lower() + random.choice(['-', '_', '.', '']) + "%s" % i + random.choice(['-', '_', '.']) + username + "@" + random.choice(['gmail.com', 'wp.pl', 'o2.pl', 'onet.pl', 'fotka.pl', 'op.pl'])
			race = random.choice(woeuserlib.races)
			sex = random.choice(woeuserlib.sexes)
			if not bot.registerAccount(username + "%s" % sufix, password + "%s" % sufix, email, nick, race, sex):
				continue
			bot.changeSettings(email, False, True)
			self.bots.append(bot)
			botCounter += 1
		print "WoeBot:registerBots()> Bots registered and logged in/all bots: %d/%d (%.2f%%) [%s .. %s]" % (botCounter, len(usernameSufix), float(botCounter) / len(usernameSufix) * 100, sufixInfo[0], sufixInfo[-1])
		return botCounter
		
	
	# bot.doTasks("bots and users") # use default tasks
	# bot.doTheseTasks("bots", doAction='princess', buyItem=('crystal_str1',2), buyItem='crystal_spd1')
	def doTasks(self, who, **tasks):
		bots = []
		i = 0
		if "user" in who:
			i = len(self.users)
			bots.extend(self.users)
		if "bot" in who:
			bots.extend(self.bots)
					
		actionDone = actionCount = 0
		boughtCount = 0
		usedCount = 0
		errorsCount = 0
		tasksCount = 0
		for bot in bots:
			if i == 0:
				bot.clearStats() # i==0 mean thats bot, not user
				
			for task in tasks if len(tasks) else bot.tasks:
				if i == 0:
					tasksCount += 1
				try:
					if type((tasks if len(tasks) else bot.tasks)[task]) in [type(()), type([])]: # if list or tuple (without set, dict, etc)
						getattr(bot, task)(*(tasks if len(tasks) else bot.tasks)[task])
					else:
						getattr(bot, task)((tasks if len(tasks) else bot.tasks)[task])
				except AttributeError: # invalid task name|| or params-not handled?
					print "WoeBot:%s> error in doTasks(): task: %s, args: %s" % (bot.getUsername(), task, (tasks if len(tasks) else bot.tasks)[task])
					if i == 0: 
						errorsCount += 1
					
			if i > 0: # if user, not bot
				print '----------\n' + bot.getNewLog()[:-1]
				i -= 1
			
			if i == 0: # if bot, not user
				if bot.lastAction != None:
					actionCount += 1
					actionDone += bot.lastAction
				boughtCount += bot.totalBoughtCount
				usedCount += bot.totalUsedCount
		
		if "bot" in who and len(self.bots):
			print '''----------
Bots' statistics:
 - count: %d [%s .. %s]''' % (len(self.bots), self.bots[0].getUsername(), self.bots[-1].getUsername()) +'''
 - task errors/all tasks: %d/%d (%.2f%% *NOT* crashed)''' % (errorsCount, tasksCount, float(tasksCount - errorsCount) / tasksCount * 100) +'''
 - actions done/all actions: %d/%d (%.2f%% Done; max 1 per bot)''' % (actionDone, actionCount, float(actionDone) / actionCount * 100) +'''
 - items bought/used: %d/%d''' % (boughtCount, usedCount)