#!/usr/bin/env python

import httplib, urllib, socket

action2location = {'applefarm': 'ponyville',
	'mail': 'ponyville',
	'bakery': 'ponyville',
	'library': 'ponyville',
	'school': 'canterlot',
	'library2': 'canterlot',
	'princess': 'canterlot',
	'garden': 'canterlot',
	'mail2': 'crystal',
	'crystalmine': 'crystal'}

name2action = {'Zbieranie jablek (Ponyville)': 'applefarm',
	'Roznoszenie poczty (Ponyville)': 'mail',
	'Wyrabianie ciastek (Ponyville)': 'bakery',
	'Pomoc w bibliotece (Ponyville)': 'library',
	'Wyklad na uniwersytecie (Canterlot)': 'school',
	'Pomoc w bibliotece (Canterlot)': 'library2',
	'Uslugiwanie ksiezniczkom (Canterlot)': 'princess',
	'Pomoc w ogrodzie (Canterlot)': 'garden',
	'Roznoszenie poczty (Crystal)': 'mail2',
	'Praca w kopalni (Crystal)': 'crystalmine'}

name2item = {'Krysztal inteligencji': 'crystal_int1', 
	'Krysztal sily': 'crystal_str1', 
	'Krysztal szybkosci': 'crystal_spd1'}

locations = ('ponyville', 'canterlot', 'crystal')

races = ('earth', 'pegasus', 'unicorn')
sexes = ('mare', 'stallion')


class WoeUser:
	def __init__(self, username="", password=""):
		self.__isLoggedIn = False
		self.__username = '*-none-*'
		self.__logStr = ""
		self.__logLastCall = 0
		self.clearStats()
		if username and password:
			self.login(username, password)
			
	def clearStats(self):
		self.lastAction = None
		self.lastBoughtCount = None
		self.lastUsedCount = None
		self.totalBoughtCount = 0
		self.totalUsedCount = 0
		
	def getLog(self):
		return self.__logStr
		
	def getNewLog(self): # get what is new from last call this func
		toReturn = self.__logStr[self.__logLastCall:]
		self.__logLastCall = len(self.__logStr)
		return toReturn
		
	def getLastLog(self): # get last line of log
		return self.__logStr.split('\n')[-1]
		
	def isLoggedIn(self):
		return self.__isLoggedIn
	
	def getUsername(self):
		return self.__username
	
	def __log(self, what):
		self.__logStr += "%s> %s\n" % (self.__username, what)
		
	def connect(self):
		self.__connection = httplib.HTTPConnection("worldofequestria.pl")
		
	def __getresponse(self):
		try:
			response = self.__connection.getresponse()
		except (httplib.HTTPException, socket.error) as ex:
			self.__log("Error: %s" % ex)
			def response():
				pass
			response.status = 408
			response.reason = "Timeout"
		return response
	
	def __getUID(self, headers):
		uid = 'none'
		for header in headers:
			if header[0].lower() == 'set-cookie': # possible only one 'set-cookie'
				uidPos = -1
				while uid == 'none':
					uidPos = header[1].find("uid=", uidPos + 1)
					if uidPos == -1:
						break
					if uidPos != 0 and header[1][uidPos-1] != ' ':
						continue
					uidEnd = header[1].find(";", uidPos + 1)
					uid = header[1][uidPos:uidEnd]
				if uid != 'none':
					break
		return uid
		
	def login(self, username, password):
		self.__username = username
		self.__isLoggedIn = False
		
		params = urllib.urlencode({'username': username, 'password': password, 'amibot': 'no'})
		self.__headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain", "Bot": "True", "User-Agent": "Mozilla/5.0 (BotOS; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0"}

		self.connect()
		self.__connection.request("POST", "/user/login", params, self.__headers)
		response = self.__getresponse()
		if response.reason == "Timeout":
			self.__log('Logging in failed. (%s %s)' % (response.status, response.reason))
			return False

		uid = self.__getUID(response.getheaders())
		if uid == 'none':
			self.__log('Logging in failed: none uid = wrong login data. (%s %s)' % (response.status, response.reason))
			return False

		self.__log('Logged in successfully. (%s %s)' % (response.status, response.reason))
		self.__headers['Cookie'] = uid
		self.__isLoggedIn = True
		return True
	
	def registerAccount(self, username, password, email, ponyname, race, sex):
		self.__username = username
		self.__isLoggedIn = False
		
		params = urllib.urlencode({'username': username, 'email': email, 'password': password, 'passwordrepeat': password, 'ponyname': ponyname, 'amibot': 'no', 'race': race, 'sex': sex})
		self.__headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain", "Bot": "True", "User-Agent": "Mozilla/5.0 (BotOS; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0"}
		
		self.connect()
		self.__connection.request("POST", "/user/register", params, self.__headers)
		response = self.__getresponse()
		if response.reason == "Timeout":
			self.__log('Registering failed. (%s %s)' % (response.status, response.reason))
			return False
			
		uid = self.__getUID(response.getheaders())
		if uid == 'none':
			self.__log('Registering failed: none uid = wrong register data. (%s %s)' % (response.status, response.reason))
			return False

		self.__log('Registered and logged in successfully. (%s %s)' % (response.status, response.reason))
		self.__headers['Cookie'] = uid
		self.__isLoggedIn = True
		return True
		
	def changeSettings(self, email, notify, hide):
		if self.__isLoggedIn == False:
			self.__log('Change settings failed. Not logged in.')
			return False
			
		params = {'email': email}
		if notify: params['settings_mails'] = 1
		if hide: params['settings_hide'] = 1
		params = urllib.urlencode(params)
		
		self.connect()
		self.__connection.request("POST", "/user/profile/settings", params, self.__headers)
		response = self.__getresponse()
		self.__log('Trying change settings: email=%s, notify=%s, hide=%s (%d %s)' % (email, notify, hide, response.status, response.reason))
		return True
		
	def travelTo(self, destinity):
		if self.__isLoggedIn == False:
			self.__log('Travel to %s failed. Not logged in.' % destinity)
			return False
		if not destinity:
			return False
		
		if not destinity in locations:
			self.__log('Warning: %s location probably not exists' % destinity)
		
		self.connect()
		self.__connection.request("POST", '/game/travel/' + destinity, '', self.__headers)
		response = self.__getresponse()
		self.__log('Travel to %s (%d %s)' % (destinity, response.status, response.reason))
		return True

	def doAction(self, action):
		if self.__isLoggedIn == False:
			self.__log('Action %s failed. Not logged in.' % action)
			return False
		if not action:
			return False
			
		if name2action.has_key(action):
			action = name2action[action]
		
		if action2location.has_key(action):
			self.travelTo(action2location[action])
		else:
			self.__log('Warning: action %s probably not exists' % action)
		
		self.connect()
		self.__connection.request("POST", '/game/action/' + action, 'do=action', self.__headers)
		response = self.__getresponse()
		
		if response.status==302 and response.reason=="Found":
			status = "Done."
		elif response.status==200 and response.reason=="OK":
			status = "Failed."
		elif response.status==408 and response.reason=="Timeout":
			status = "Request Timeout."
		else:
			status = "Unknown status."
		
		self.lastAction = status == "Done."
		self.__log('Do action %s: %s (%d %s)' % (action, status, response.status, response.reason))
		return True
		
	def buyItem(self, item, amountToBuy=0):
		if self.__isLoggedIn == False:
			self.__log('Buying %s failed. Not logged in.' % item)
			return False
		if not item:
			return False
		
		if name2item.has_key(item):
			item = name2item[item]
		
		canMore = True
		always = amountToBuy == 0
		boughtCount = 0
		
		while canMore and (always or boughtCount < amountToBuy):
			self.connect()
			self.__connection.request("POST", '/game/shop/buy/' + item, 'do=true', self.__headers)
			response = self.__getresponse()
			if response.status != 302:
				canMore = False
			else:
				for header in response.getheaders():
					if header[0] == 'location':
						if header[1] != 'http://worldofequestria.pl/game/inventory?key=%s&status=201' % item:
							canMore = False
				if canMore:
					boughtCount += 1
		
		self.lastBoughtCount = boughtCount
		self.totalBoughtCount += boughtCount
		self.__log('Buying item %s (%s times): %d bought (last POST: %d %s)' % (item, 'max' if always else amountToBuy, boughtCount, response.status, response.reason))
		return True #(True, boughtCount)
	
	def useItem(self, item, amountToUse=0):
		if self.__isLoggedIn == False:
			self.__log('Using %s failed. Not logged in.' % item)
			return False
		if not item:
			return False
		
		if name2item.has_key(item):
			item = name2item[item]
		
		canMore = True
		always = amountToUse == 0
		usedCount = 0
		
		while canMore and (always or usedCount < amountToUse):
			self.connect()
			self.__connection.request("POST", '/game/inventory/use/' + item, '', self.__headers)
			response = self.__getresponse()
			if response.status != 302:
				canMore = False
			else:
				for header in response.getheaders():
					if header[0] == 'location':
						if header[1] != 'http://worldofequestria.pl/game/inventory?key=%s&status=200' % item:
							canMore = False
				if canMore:
					usedCount += 1
		
		self.lastUsedCount = usedCount
		self.totalUsedCount += usedCount
		self.__log('Using item %s (%s times): %d used (last POST: %d %s)' % (item, 'max' if always else amountToUse, usedCount, response.status, response.reason))
		return True #(True, usedCount)
